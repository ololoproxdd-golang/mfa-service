package server

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/config"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/mfa"
	health_storage "gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/health"
	mfa_storage "gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/mfa"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/transport"
	"google.golang.org/grpc"
	"sync"
)

type server struct {
	grpcServer *grpc.Server

	config    config.Config
	endpoints *transport.MfaEndpoints

	healthService health.HealthService
	hotpService   mfa.MfaService
	totpService   mfa.MfaService

	healthStorage health_storage.HealthStorage
	mfaStorage    mfa_storage.MfaStorage

	redisClient *redis.Client
}

func NewServer(config config.Config) *server {
	return &server{config: config}
}

// close all descriptors
func (s *server) Close() {
	if err := s.redisClient.Close(); err != nil {
		log.Info().Err(err).Msg("closing redis connect error")
	}

	s.grpcServer.Stop()
}

// open init structure
func (s *server) Open() error {
	s.initLogger()
	s.registerOsSignal()

	return nil
}

func (s *server) Run() {
	wg := sync.WaitGroup{}

	wg.Add(1)
	go s.runGrpcServer(&wg)

	wg.Wait()
	log.Info().Msg("mfa-service stopped")
}
