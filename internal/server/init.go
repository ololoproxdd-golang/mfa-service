package server

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/mfa"
	health_storage "gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/health"
	mfa_storage "gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/mfa"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/transport"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"time"
)

func (s *server) getRedisClient() *redis.Client {
	if s.redisClient != nil {
		return s.redisClient
	}

	s.redisClient = redis.NewClient(
		&redis.Options{
			Addr:            s.config.Redis,
			MaxRetries:      3,
			MaxRetryBackoff: 100 * time.Millisecond,
			ReadTimeout:     50 * time.Millisecond,
			WriteTimeout:    50 * time.Millisecond,

			PoolSize:     200,
			MinIdleConns: 80,
			PoolTimeout:  100 * time.Millisecond,
		},
	)

	if _, err := s.redisClient.Ping().Result(); err != nil {
		log.Fatal().Err(err).Msg("redis connection failed")
	}
	log.Info().Msg("created redis client")

	return s.redisClient
}

func (s *server) getHealthStorage() health_storage.HealthStorage {
	if s.healthStorage != nil {
		return s.healthStorage
	}

	s.healthStorage = health_storage.NewHealthStorage(s.getRedisClient())
	log.Info().Msg("created health storage")

	return s.healthStorage
}

func (s *server) getMfaStorage() mfa_storage.MfaStorage {
	if s.mfaStorage != nil {
		return s.mfaStorage
	}

	s.mfaStorage = mfa_storage.NewRedisStorage(s.config, s.getRedisClient())
	log.Info().Msg("created otp storage")

	return s.mfaStorage
}

func (s *server) getHotpService() mfa.MfaService {
	if s.hotpService != nil {
		return s.hotpService
	}

	s.hotpService = mfa.NewHotpService(s.config, s.getMfaStorage())
	log.Info().Msg("created HOTP service")

	return s.hotpService
}

func (s *server) getTotpService() mfa.MfaService {
	if s.totpService != nil {
		return s.totpService
	}

	s.totpService = mfa.NewTotpService(s.config, s.getMfaStorage())
	log.Info().Msg("created TOTP service")

	return s.totpService
}

func (s *server) getHealthService() health.HealthService {
	if s.healthService != nil {
		return s.healthService
	}
	s.healthService = health.NewHealthService(s.getHealthStorage())
	log.Info().Msg("created health service")

	return s.healthService
}

func (s *server) getEndpoints() *transport.MfaEndpoints {
	if s.endpoints != nil {
		return s.endpoints
	}

	s.endpoints = transport.NewEndpoints(s.getHotpService(), s.getTotpService(), s.getHealthService())
	log.Info().Msg("created server endpoints")
	return s.endpoints

}

func (s *server) getTransportGrpc() *grpc.Server {
	if s.grpcServer != nil {
		return s.grpcServer
	}

	s.grpcServer = grpc.NewServer(grpc.UnaryInterceptor(GrpcLoggerInterceptor))
	v1.RegisterMfaServiceServer(s.grpcServer, transport.MakeGrpcServer(s.getEndpoints()))

	// Register reflection service on gRPC server.
	reflection.Register(s.grpcServer)
	log.Info().Msg("created transport - grpc")

	return s.grpcServer
}
