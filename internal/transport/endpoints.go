package transport

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/health"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/service/mfa"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type MfaEndpoints struct {
	Ping   endpoint.Endpoint
	Health endpoint.Endpoint
	Create endpoint.Endpoint
	Check  endpoint.Endpoint
}

func NewEndpoints(hotp, totp mfa.MfaService, health health.HealthService) *MfaEndpoints {
	e := &MfaEndpoints{
		Ping:   makePingEndpoint(health),
		Health: makeHealthEndpoint(health),
		Create: makeCreateEndpoint(hotp, totp),
		Check:  makeCheckEndpoint(hotp, totp),
	}
	return e
}

func makePingEndpoint(health health.HealthService) endpoint.Endpoint {
	return func(context.Context, interface{}) (response interface{}, err error) {
		return health.Ping()
	}
}

func makeHealthEndpoint(health health.HealthService) endpoint.Endpoint {
	return func(context.Context, interface{}) (response interface{}, err error) {
		return health.Health()
	}
}

func makeCreateEndpoint(hotp, totp mfa.MfaService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req, ok := request.(v1.CreateRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCreateEndpoint not type of v1.CreateRequest")
		}

		switch req.Type {
		case v1.MfaType_HOTP:
			return hotp.Create(req)
		case v1.MfaType_TOTP:
			return totp.Create(req)
		default:
			return nil, errors.New("MFA type not support")
		}
	}
}

func makeCheckEndpoint(hotp, totp mfa.MfaService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req, ok := request.(v1.CheckRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCheckEndpoint not type of v1.CheckRequest")
		}

		switch req.Type {
		case v1.MfaType_HOTP:
			return hotp.Check(req)
		case v1.MfaType_TOTP:
			return totp.Check(req)
		default:
			return nil, errors.New("MFA type not support")
		}
	}
}
