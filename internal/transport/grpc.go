package transport

import (
	"context"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"github.com/golang/protobuf/ptypes/empty"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type GRPCServer struct {
	ping   kitgrpc.Handler
	health kitgrpc.Handler
	create kitgrpc.Handler
	check  kitgrpc.Handler
}

func MakeGrpcServer(endpoints *MfaEndpoints) v1.MfaServiceServer {
	opts := []kitgrpc.ServerOption{
		kitgrpc.ServerErrorHandler(NewErrorHandler("grpc")),
	}

	return &GRPCServer{
		ping: kitgrpc.NewServer(
			endpoints.Ping,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		health: kitgrpc.NewServer(
			endpoints.Health,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		create: kitgrpc.NewServer(
			endpoints.Create,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
		check: kitgrpc.NewServer(
			endpoints.Check,
			noEncodeDecode,
			noEncodeDecode,
			opts...),
	}
}

// function not encoding
func noEncodeDecode(_ context.Context, r interface{}) (interface{}, error) {
	return r, nil
}

func (s *GRPCServer) Ping(ctx context.Context, req *empty.Empty) (*v1.PingResponse, error) {
	_, resp, err := s.ping.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.PingResponse), nil
}

func (s *GRPCServer) Health(ctx context.Context, req *empty.Empty) (*v1.HealthResponse, error) {
	_, resp, err := s.health.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	res := resp.(*v1.HealthResponse)

	return res, nil
}

func (s *GRPCServer) Create(ctx context.Context, req *v1.CreateRequest) (*v1.CreateResponse, error) {
	_, resp, err := s.create.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.CreateResponse), nil
}

func (s *GRPCServer) Check(ctx context.Context, req *v1.CheckRequest) (*v1.CheckResponse, error) {
	_, resp, err := s.check.ServeGRPC(ctx, *req)
	if err != nil {
		return nil, err
	}

	return resp.(*v1.CheckResponse), nil
}
