package mfa

import (
	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/config"
	"time"
)

type redisStorage struct {
	cfg         config.Config
	redisClient *redis.Client
}

func NewRedisStorage(
	cfg config.Config,
	redisClient *redis.Client,
) *redisStorage {
	return &redisStorage{
		cfg:         cfg,
		redisClient: redisClient,
	}
}

func (s *redisStorage) Add(key string, value interface{}, exp time.Duration) error {
	if _, err := s.redisClient.Set(
		key,
		value,
		exp,
	).Result(); err != nil {
		return err
	}

	return nil
}

func (s *redisStorage) Get(key string) (interface{}, error) {
	value, err := s.redisClient.Get(key).Result()
	if err != nil {
		log.Info().Err(err).Msgf("Redis storage find not %s key", key)
		return "", NotFountErr
	}

	return value, nil
}
