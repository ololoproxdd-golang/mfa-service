package mfa

import (
	"github.com/pkg/errors"
	"time"
)

const (
	SECRET_STORAGE_KEY  = "secret"
	QR_STORAGE_KEY      = "qr"
	COUNTER_STORAGE_KEY = "counter"
)

var (
	NotFountErr = errors.New("not found key in storage")
)

type MfaStorage interface {
	Add(string, interface{}, time.Duration) error
	Get(string) (interface{}, error)
}
