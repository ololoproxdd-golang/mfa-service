package health

import (
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type healthStorage struct {
	redisClient *redis.Client
}

func NewHealthStorage(
	redisClient *redis.Client,
) *healthStorage {
	return &healthStorage{
		redisClient: redisClient,
	}
}

func (hs *healthStorage) CheckConnection() error {
	if err := hs.redisClient.Ping().Err(); err != nil {
		return errors.Wrap(err, "Ping redis failed")
	}

	return nil
}
