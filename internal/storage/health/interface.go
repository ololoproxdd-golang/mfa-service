package health

type HealthStorage interface {
	CheckConnection() error
}
