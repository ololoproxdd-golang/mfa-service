package mfa

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/pkg/errors"
	"github.com/pquerna/otp/totp"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/config"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/mfa"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
	"image/png"
)

const (
	ISSUER = "MFA service"
	HEIGHT = 200
	WIDTH  = 200
)

type totpService struct {
	cfg     config.Config
	storage mfa.MfaStorage
}

func NewTotpService(cfg config.Config, storage mfa.MfaStorage) *totpService {
	return &totpService{
		cfg:     cfg,
		storage: storage,
	}
}

func (s *totpService) Create(req v1.CreateRequest) (*v1.CreateResponse, error) {
	qrKey, err := totp.Generate(totp.GenerateOpts{
		Issuer:      ISSUER,
		AccountName: req.Email,
	})
	if err != nil {
		return nil, errors.Wrap(err, "can't create QR key")
	}

	img, err := qrKey.Image(WIDTH, HEIGHT)
	if err != nil {
		return nil, errors.Wrap(err, "can't create QR key")
	}

	var buf bytes.Buffer
	err = png.Encode(&buf, img)
	if err != nil {
		return nil, errors.Wrap(err, "Encoding error")
	}
	imgBase64Str := base64.StdEncoding.EncodeToString(buf.Bytes())

	key := fmt.Sprintf("%s:%s", mfa.QR_STORAGE_KEY, req.Email)
	err = s.storage.Add(key, qrKey.Secret(), 0)
	if err != nil {
		return nil, err
	}

	return &v1.CreateResponse{
		Message: "success create QR key",
		Value:   imgBase64Str,
	}, nil
}

func (s *totpService) Check(req v1.CheckRequest) (*v1.CheckResponse, error) {
	key := fmt.Sprintf("%s:%s", mfa.QR_STORAGE_KEY, req.Email)
	qrSecret, err := s.storage.Get(key)
	if err != nil {
		return nil, err
	}

	if !totp.Validate(req.Value, qrSecret.(string)) {
		return nil, checkError
	}

	return &v1.CheckResponse{
		Message: "totp is valid",
	}, nil
}
