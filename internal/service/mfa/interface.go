package mfa

import (
	"github.com/pkg/errors"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

var (
	checkError = errors.New("OTP not valid")
)

type MfaService interface {
	Create(v1.CreateRequest) (*v1.CreateResponse, error)
	Check(v1.CheckRequest) (*v1.CheckResponse, error)
}
