package mfa

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/pquerna/otp/hotp"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/config"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/mfa"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
	"math/rand"
	"strconv"
	"time"
)

const (
	secretLength = 20 // 160 bit
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

type hotpService struct {
	cfg     config.Config
	storage mfa.MfaStorage
}

func NewHotpService(cfg config.Config, storage mfa.MfaStorage) *hotpService {
	return &hotpService{
		cfg:     cfg,
		storage: storage,
	}
}

func (s *hotpService) randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func (s *hotpService) get(key string) (interface{}, error) {
	value, err := s.storage.Get(key)
	if err != nil {
		return "", err
	}

	return value, nil
}

func (s *hotpService) getSecret(mail string) (interface{}, error) {
	key := fmt.Sprintf("%s:%s", mfa.SECRET_STORAGE_KEY, mail)
	return s.get(key)
}

func (s *hotpService) getCounter(mail string) (interface{}, error) {
	key := fmt.Sprintf("%s:%s", mfa.COUNTER_STORAGE_KEY, mail)
	return s.get(key)
}

func (s *hotpService) create(key string, value interface{}, duration time.Duration) (interface{}, error) {
	err := s.storage.Add(key, value, duration)
	if err != nil {
		return "", err
	}

	return value, nil
}

func (s *hotpService) createSecret(mail string) (interface{}, error) {
	key := fmt.Sprintf("%s:%s", mfa.SECRET_STORAGE_KEY, mail)
	value := s.randStringRunes(secretLength)
	return s.create(key, value, 0)
}

func (s *hotpService) createCounter(mail string, value interface{}) (interface{}, error) {
	key := fmt.Sprintf("%s:%s", mfa.COUNTER_STORAGE_KEY, mail)
	return s.create(key, value, 0)
}

func (s *hotpService) Create(req v1.CreateRequest) (*v1.CreateResponse, error) {
	secret, sErr := s.getSecret(req.Email)
	counter, cErr := s.getCounter(req.Email)
	switch {
	case cErr == nil && sErr == nil:
		break
	case cErr == mfa.NotFountErr && sErr == mfa.NotFountErr:
		secret, sErr = s.createSecret(req.Email)
		counter, cErr = s.createCounter(req.Email, "0")
		if sErr == nil && cErr == nil {
			goto next
		}
		fallthrough
	default:
		return nil, errors.Errorf("Create HOTP errors: [%s, %s]", sErr, cErr)
	}

next:
	c, err := strconv.Atoi(counter.(string))
	if err != nil {
		return nil, errors.Wrap(err, "strconv Atoi error")
	}

	code, err := hotp.GenerateCode(secret.(string), uint64(c))
	if err != nil {
		return nil, errors.Wrap(err, "HOTP generate code error")
	}

	return &v1.CreateResponse{
		Message: "success create otp",
		Value:   code,
	}, nil
}

func (s *hotpService) Check(req v1.CheckRequest) (*v1.CheckResponse, error) {
	secret, sErr := s.getSecret(req.Email)
	counter, cErr := s.getCounter(req.Email)
	if sErr != nil || cErr != nil {
		return nil, errors.Errorf("Get secret, counter errors: [%s, %s]", sErr, cErr)
	}

	c, err := strconv.Atoi(counter.(string))
	if err != nil {
		return nil, errors.Wrap(err, "strconv Atoi error")
	}

	if !hotp.Validate(req.Value, uint64(c), secret.(string)) {
		return nil, checkError
	}

	//увеличиваем счетик
	_, cErr = s.createCounter(req.Email, c+1)
	if cErr != nil {
		return nil, errors.Wrap(err, "Append counter error")
	}

	return &v1.CheckResponse{
		Message: "success, otp is equal request value",
	}, nil
}
