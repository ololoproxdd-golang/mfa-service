package health

import (
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type HealthService interface {
	Ping() (*v1.PingResponse, error)
	Health() (*v1.HealthResponse, error)
}
