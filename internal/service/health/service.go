package health

import (
	"github.com/pkg/errors"
	"gitlab.com/ololoproxdd-golang/mfa-service/internal/storage/health"
	v1 "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type healthService struct {
	storage health.HealthStorage
}

func NewHealthService(storage health.HealthStorage) *healthService {
	return &healthService{
		storage: storage,
	}
}

func (h healthService) Ping() (*v1.PingResponse, error) {
	return &v1.PingResponse{Message: "Pong"}, nil
}

func (h healthService) Health() (*v1.HealthResponse, error) {
	if err := h.storage.CheckConnection(); err != nil {
		return nil, errors.Wrap(err, "Health check to storage error")
	}

	return &v1.HealthResponse{Message: "Status: Ok"}, nil
}
